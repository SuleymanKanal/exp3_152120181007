/*
 * Circle.cpp
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */

#include <iostream>
#include "Circle.h"

Circle::Circle(double r) {
	setR(r);
}

Circle::~Circle() {
}

void Circle::setR(double r){
	this->r = r; 
}

double Circle::getR() const{
	return r;
}

double Circle::calculateCircumference() const{
	return PI * r * 2;
}

double Circle::calculateArea(){
	return PI * r * r;
}

bool Circle::equals(Circle a)
{
	if (a.r == this->r)
	{
		std::cout << "Circles are equal" << std::endl;
		return true;
	}
	else
	{
		std::cout << "Circles are not equal" << std::endl;
		return false;
	}
}

void Circle::setR(int r)
{
	setR(double(r));
}